#ifndef _I2C_H
#define _I2C_H

void i2cStart( void );
void i2cStop( void );
void i2cSend( unsigned char j);

#endif
