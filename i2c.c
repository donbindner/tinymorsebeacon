/* Copyright (C) 2015 Donald J. Bindner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * i2c.c
 * Provides basic I2C serial communications via bit-banging.
 */

#include <msp430.h>

#define I2C_DAT BIT2	// Data line
#define I2C_CLK BIT5	// Clock line

void dataHigh(void ) {
    P1DIR &= ~I2C_DAT;	// float to go high
    delay10us( 1 );
}

void dataLow(void ) { 
    P1OUT &= ~I2C_DAT;	// assert low
    P1DIR |= I2C_DAT;
    delay10us( 1 );
}

void clockHigh(void) {
    int n;
    P1DIR &= ~I2C_CLK;	// float to go high

    // Wait if necessary for slave to stretch the clock
    for( n = 0; n < 1000; n++ ) {
	if( P1IN & I2C_CLK ) break;
    }

    // Small extra delay after the clock goes high
    delay10us( 1 );
}

void clockLow(void) {
    P1OUT &= ~I2C_CLK;	// assert low
    P1DIR |= I2C_CLK;
    delay10us( 1 );
}

/* I2C communication starts when both the data and clock lines go low,
 * in order. */
void i2cStart(void) {
    clockHigh();	
    dataHigh();
    dataLow();
    clockLow();
}

/* I2C communication stops with both the clock and data lines going high,
 * in order. */
void i2cStop(void) {
    dataLow();
    clockLow();
    clockHigh();
    dataHigh();
}

/* Outputs command or data via I2C lines */
void i2cSend(unsigned char j) {
    int n;
    unsigned char d = j;

    for( n = 0; n < 8; n++ ) {
	if( d & 0x80 ) {
	    dataHigh();	
	} else {
	    dataLow();	
	}

	d <<= 1;		// Shift 1 left for next time through the loop

	clockHigh();	// Clock line goes high
	clockLow();	// Clock line goes low
    }

    dataHigh();	// Set data line high to receive
    clockHigh();		// Clock goes high to wait for acknowledge

    // Hopefully this goes okay, but we'll stop after 1000 tries if not.
    for( n = 0; n < 1000; n++ ) {
	// slave will pull data line low to acknowledge.
       	if(( P1IN & I2C_DAT ) == 0 ) break;

	// Else toggle the clock and try again
	clockLow();	// Clock goes low
	clockHigh();	// Clock line goes high
    }

    clockLow();		// Clock line goes low.
}
