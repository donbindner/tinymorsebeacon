#!/usr/bin/python3

# Copyright (C) 2015 Donald J. Bindner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

totaltime=400       # must be a multiple of 8
msg='MOE '          # repeated to fill out message
call='DE N0CALL N0CALL K'   # given at the end of the minute

encoding = {
'A':'.- ',
'B':'-... ',
'C':'-.-. ',
'D':'-.. ',
'E':'. ',
'F':'..-. ',
'G':'--. ',
'H':'.... ',
'I':'.. ',
'J':'.--- ',
'K':'-.- ',
'L':'.-.. ',
'M':'-- ',
'N':'-. ',
'O':'--- ',
'P':'.--. ',
'Q':'--.- ',
'R':'.-. ',
'S':'... ',
'T':'- ',
'U':'..- ',
'V':'...- ',
'W':'.-- ',
'X':'-..- ',
'Y':'-.-- ',
'Z':'--.. ',
'1':'.---- ',
'2':'..--- ',
'3':'...-- ',
'4':'....- ',
'5':'..... ',
'6':'-.... ',
'7':'--... ',
'8':'---.. ',
'9':'----. ',
'0':'----- ',
' ':'  ',
}

# convert the string s to string of dots and dashes, character spaces, and word
# spaces
def string2dots( s ):
    dots = ''

    for c in s:
        dots += encoding[c]

    return dots

# calculate the time of message in dot units
# . = 2 (one dot of tone plus one dot of symbol space)
# - = 4 (3 dots of tone plus one dot of symbol space)
# space = 2 (counting one dot already out, two more makes 3 of char space)
def timeof( dots ):
    total = 0

    for c in dots:
        if c=='.':
            total += 2
        elif c=='-':
            total += 4
        elif c==' ':
            total += 2
        else:
            print( "Unexpected symbol: " + c )

    return total

# Convert message from dots and dashes to string of 1=tone and 0=silence
def dots2markspace( dots ):
    markspace = ''

    for c in dots:
        if c=='.':
            markspace += '10'
        elif c=='-':
            markspace += '1110'
        elif c==' ':
            markspace += '00'
        else:
            print( "Unexpected symbol: " + c )

    return markspace

# Convert string of 0s and 1s to list of bites
def markspace2bits( markspace ):
    b = []
    count = 0
    tmp = 0

    for c in markspace:
        tmp = tmp >> 1
        if c=='1':
            tmp = tmp | 0x80
        count += 1

        if count==8:
            b.append(tmp)
            tmp=0
            count=0

    return b

### Main program begins here ###

# Convert message to dots and dashes
msgdots = string2dots( msg );
calldots = string2dots( call );

# Calculate time in dots of message and call
msgtime = timeof( msgdots );
calltime = timeof( calldots );

# Compute the number of repeats we can fit into one minute
repeats = ( totaltime - calltime ) // msgtime

# Convert message and call sign to string of 1s (where tone happens)
# and 0s (where silence happens)
oneszeros = ''
for i in range( repeats ):
    oneszeros += dots2markspace( msgdots )
oneszeros += dots2markspace( calldots )

# Pad with silence to fill out the full minute
padlength = totaltime - len( oneszeros )
padfront = padlength // 2
padback = padlength - padfront
oneszeros = '0'*padfront + oneszeros + '0'*padback

# Convert to array of bytes
msgbytes = markspace2bits( oneszeros )

# Print out bytes as C header
print( "/* " + msg + call + " */" )
print( "#define MSGLENGTH {:d}".format(len(msgbytes)))
print( "unsigned char msgbytes[MSGLENGTH] = {" )
for i in msgbytes:
    print( "0x{:02x},".format(i) )
print( "};" )
