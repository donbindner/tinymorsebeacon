#ifndef _SI5351_h
#define _SI5351_h

void i2cSendRegister( unsigned char reg, unsigned char data );
void i2cInitializeRegisters();

#endif
