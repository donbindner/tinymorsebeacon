/* Copyright (C) 2015 Donald J. Bindner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * morse.c
 * Routines for creating the audio of the Morse code message. */
#include <msp430.h>
#include "morse.h"
#include "message.h"

/* The submain clock runs at 1MHz.  Using a /8 divider, gives
 * a clock tick of 125KHz.  If we set CCR0 to 179, this defines
 * an audio frequency of 125KHz/180 = 694Hz.  In Morse code, each
 * "word" is 50 dots long, so 8 WPM is 400 dots/min.  That makes
 * the length of each dot almost exactly 104 full cycles. */

#define AUDIO_OUT BIT6
#define TICKS 179
#define DOTLENGTH 104
#define HFREQ 8
#define HLRATIO (TICKS+1)/(HFREQ+1)

volatile int cycles = 0;
volatile int msgcount = 0;
volatile unsigned char mask = 0x01;

/* Set's the main clock to 1 MHz.  We use this calibrated speed
 * so that our tones (which are derived from the main clock) have
 * the right pitch, and our dots have the right duration. */
void setMainClockTo1MHz() {
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;
}

void initializePWMtimer() {
    TA0CCTL1 = OUTMOD_7;	/* CCR1 reset/set -- high until CCR1 is reached */
    TACTL = TASSEL_2 + MC_1 + ID_3;	/* submain clock in count-up mode divide by 8 */
    TA0CCR0 = 0xFFFF;		/* dummy value */
}

/* Turns on the tone by setting a count-up value to TICKS.  If we were
 * already playing a tone, then this value will already have been set
 * and we don't have to do anything. */
void turnToneOn() {
    if( TA0CCR0 == TICKS ) return;	/* nothing to do */

    TA0CCR0 = TICKS;		/* Sets the PWM period. */
    TA0CCR1 = (TICKS+1)/2;	/* Sets the duty cycle to 50%. */
}

/* Turns off the tone by setting a count-up valuet to HFREQ (which is
 * quite short, and causes the pulse width modulation to generate a
 * very high frequency.  After passing through a low-pass filter on
 * the circuit board, this will generate silence. */
void turnSilenceOn() {
    if( TA0CCR0 == HFREQ ) return;	/* nothing to do */

    TA0CCR0 = HFREQ;		/* Sets the PWM period. */
    TA0CCR1 = (HFREQ+1)/2;	        /* Sets the duty cycle to 50%. */
}

/* Enables the audio pin to do pulse width modulation. */
void enablePWMpin() {
    P1SEL |= AUDIO_OUT;	/* Select for PWM transition at comparison to CCR1 */
    P1DIR |= AUDIO_OUT; /* Set as output */
}

/* Sets the audio pin as a standard output pin. */
void disablePWMpin() {
    P1SEL &= ~AUDIO_OUT; /* Turn off PWM for pin. */
    P1OUT &= ~AUDIO_OUT; /* Set pin low to save power. */
}

void startMorseMessage() {
    msgcount = 0;	/* Start in the first byte of the message */
    mask = 1;		/* Start at the first bit of the byte */
    cycles = 0;		/* Reset the interrupt cycles counter */

    TACCTL0 &= ~CCIFG;	/* clear the interrupt flag */
    TACCTL0 |= CCIE;	/* Enable interrupts */
}

/* This interrupt is called after every cycle in the pulse width
 * modulation.  It keeps track of where in each dot interval we are
 * and moves through the message, turning the tone one (in dots and
 * dashes) and off (in spaces between letters and words). */
void Timer_A_isr(void) __attribute__((interrupt(TIMER0_A0_VECTOR)));
void Timer_A_isr(void) {
    TACCTL0 &= ~CCIFG;	/* clear the interrupt flag */
 
    if( cycles == 0 ) {
	if( msgcount >= MSGLENGTH ) {
	    TACCTL0 &= ~CCIE;	/* disable interrupts, and */
	    turnSilenceOn();	/* At end of message, silence, */
	    __bic_SR_register_on_exit(LPM0_bits);	/* wake up the main program */
	    return;
	} else if( msgbytes[msgcount] & mask ) {
	    turnToneOn();
	    cycles = DOTLENGTH;	/* ticks until next dot interval */
	} else {
	    turnSilenceOn();
	    cycles = DOTLENGTH*HLRATIO;	/* at higher frequency it takes more ticks */
	}

	mask <<= 1;
	if( mask == 0x00 ) {
	    mask = 0x01;
	    msgcount++;
	}
    }

    cycles--;
}
