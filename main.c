/* Copyright (C) 2015 Donald J. Bindner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * main.c
 * Main program.  Wake up every 5 minutes and transmit a Morse
 * code message. */

#include <msp430.h>
#include "morse.h"
#include "si5351.h"
#include "i2c.h"
#include "delay.h"

#define FIVEMINUTERESET 102	/* determine impirically -- nominally should be 110 */
volatile int fiveMinuteCounter = FIVEMINUTERESET;

#define RADIOPWR BIT7	/* this pin controls the power to the radio */

void powerOnRadio() {
    P1DIR |= RADIOPWR;
    P1OUT &= ~RADIOPWR;		/* active low */
}

void powerOffRadio() {
    P1DIR |= RADIOPWR;
    P1OUT |= RADIOPWR;		/* inactive high */
}

int main(void) {
    int count = 0;

    /* Set the watchdog timer to use the VLO and generate interrupts
     * every 2731ms.  Then 5 minutes is about 110 ticks. */
    BCSCTL3 |= LFXT1S_2;	/* Set ACLK to use VLO */
    WDTCTL = WDT_ADLY_1000;	/* This is really 1000ms*32.768KHz/12KHz */
    IFG1 &= ~WDTIFG;	/* clear interrupt flag */
    IE1 |= WDTIE;	/* enable interrupts */

    setMainClockTo1MHz();
    initializePWMtimer();

    while( 1 ) {
	turnSilenceOn();
	enablePWMpin();

	powerOnRadio();
	delay10us(1000);	/* 10 milliseconds for chip to come up */
	i2cInitializeRegisters();

	/* queue up message and then sleep until sent */
	startMorseMessage();
	__bis_SR_register( LPM0_bits + GIE );
	
	powerOffRadio();
	disablePWMpin();

	/* sleep for 4 minutes in deep sleep */
	__bis_SR_register( LPM3_bits + GIE );
    }
}

/* Five minute countdown timer. */
void WDT_ISR(void) __attribute__((interrupt(WDT_VECTOR)));
void WDT_ISR(void)
{
    if( fiveMinuteCounter == 0 ) {
	fiveMinuteCounter = FIVEMINUTERESET;
	__bic_SR_register_on_exit( LPM3_bits );		/* wake up the main program */
    } else {
	fiveMinuteCounter--;
    }

    /* clear the interrupt flag for watchdog timer */
    IFG1 &= ~WDTIFG;
}
