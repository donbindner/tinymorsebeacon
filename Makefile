MCU=msp430g2210
CC=msp430-gcc
MSPDEBUG=mspdebug
FLAGS=-O2

main.elf: main.o morse.o si5351.o i2c.o delay.o
	$(CC) -mmcu=$(MCU) $(FLAGS) -o main.elf main.o morse.o si5351.o i2c.o delay.o
	msp430-size main.elf

main.o: main.c morse.h si5351.h i2c.h delay.h
	$(CC) -mmcu=$(MCU) $(FLAGS) -c -o main.o main.c

morse.o: morse.c morse.h message.h
	$(CC) -mmcu=$(MCU) $(FLAGS) -c -o morse.o morse.c

# It's necessary to edit encoder.py to insert your own MO-* message and call sign.
message.h: encoder.py
	python encoder.py > message.h

# Choose register_map.h values with the Si5351 ClockBuilder to get on frequency.
si5351.o: si5351.c si5351.h register_map.h
	$(CC) -mmcu=$(MCU) $(FLAGS) -c -o si5351.o si5351.c

i2c.o: i2c.c i2c.h
	$(CC) -mmcu=$(MCU) $(FLAGS) -c -o i2c.o i2c.c

delay.o: delay.s delay.h
	msp430-as -mmcu=$(MCU) -o delay.o delay.s

install: main.elf
	$(MSPDEBUG) rf2500 "prog main.elf"

clean:
	rm -f *.bak *.o message.h main.elf
