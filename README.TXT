Configuring and Compiling the Morse Code Beacon

Two things need to be done to get the Morse Code Beacon up and
running.
 - The message payload needs to be set.
 - The frequency needs to be set and tuned. 

Message Payload
~~~~~~~~~~~~~~~

Setting the message payload is the quickest and easiest task.  Open
the helper programe 'encoder.py' and edit the two variables at the top
to set the message and call sign.  The default message is "MOE " and
the default call sign is "N0CALL".

The message length can also be tuned.  It is measured in 'dot lengths'
and the default is 400, which is approximately 1 minute of
transmission time at the default speed of 8 wpm.

The program will convert the message to binary ones and zeros and
store them in an array of characters in message.h that gets compiled
into the main program.  Ones represent tone and zeros are silence.


Setting the Frequency
~~~~~~~~~~~~~~~~~~~~~

The transmit frequency is set in register_map.h, which is generated by
the Si5351 ClockBuilder program (which is unfortunately Windows-only).
That program lets you output a C header file, which our program uses
with only minor modification.

Because each actual device will differ slightly in its
characteristics, a header that is designed for a particular frequency
will be close, but almost certainly not right on.  For example, if
your header is designed for 146.565MHz, it might actually produce a
signal at 146.559 MHz.  A receiver with a "discriminator center meter"
can easily verify this.

If that happens, you use the ratio between your desired frequency and
actual frequency to come up with a new design frequency.  For example:

146.565 * ( 146.565/146.559 ) = 146.571

I would generate a new header file using a design frequency of
146.571MHz, and that should produce a proper signal at 146.565MHz for
your particular hardware.
