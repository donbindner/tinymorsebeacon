/* Copyright (C) 2015 Donald J. Bindner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * si5351.c
 * Routines for sending data from the register map to the si5351
 * signal generator. */

#include <msp430.h>
#include "si5351.h"
#include "i2c.h"
#include "register_map.h"

#define I2C_WRITE 0b11000000
#define  I2C_READ 0b11000001
#define SI_PLL_RESET 177

void i2cSendRegister( unsigned char reg, unsigned char data ) {
    i2cStart();
    i2cSend( I2C_WRITE );
    i2cSend( reg );
    i2cSend( data );
    i2cStop();
}

void i2cInitializeRegisters() {
    int i;

    for( i = 0; i < NUM_REGS_MAX; i++ ) {
	i2cSendRegister( Reg_Store[i].Reg_Addr, Reg_Store[i].Reg_Val );
    }
}
